FROM ubuntu
#NGINX-UBUNTIU
RUN apt-get update -y && \
    apt-get install wget unzip vim  nettools* sudo openssh-client curl -y && \
    apt-get install nginx -y
WORKDIR /etc/nginx
#CMD service nginx start

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
